<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "preguntas".
 *
 * @property integer $id
 * @property string $descripcion
 * @property integer $calificacion
 * @property integer $id_dimension
 * @property integer $id_dominio
 */
class Preguntas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'preguntas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['calificacion', 'id_dimension', 'id_dominio'], 'integer'],
            [['descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'calificacion' => 'Calificacion',
            'id_dimension' => 'Id Dimension',
            'id_dominio' => 'Id Dominio',
        ];
    }
}
