<?php
namespace common\models\nodo;

use Yii;

/**
 * This is the model class for table "nodo_tipo".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $folder
 * @property integer $borrado
 */
class NodoTipo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nodo_tipo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['borrado'], 'integer'],
            [['nombre', 'descripcion', 'folder'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'folder' => 'Folder',
            'borrado' => 'Borrado',
        ];
    }

}
