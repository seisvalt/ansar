<?php

namespace common\models\nodo;

use Yii;

/**
 * This is the model class for table "nodo".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $latitud
 * @property string $longitud
 * @property string $direccion
 * @property integer $id_zona
 * @property string $proyectos
 * @property integer $estado
 *
 * @property Zonas $idZona
 */
class Nodo extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nodo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['latitud', 'longitud'], 'number'],
            [['id_zona', 'id_tipo', 'estado'], 'integer'],
            ['estado', 'default', 'value' => self::STATUS_ACTIVE],
            ['estado', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['nombre', 'descripcion', 'direccion', 'proyectos'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'direccion' => 'Direccion',
            'id_zona' => 'Zona',
            'id_tipo' => 'Tipo',
            'proyectos' => 'Proyectos',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdZona()
    {
        return $this->hasOne(Zonas::className(), ['id' => 'id_zona']);
    }
}
