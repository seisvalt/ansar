<?php

namespace common\models\nodo;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\nodo\NodoTipo;

/**
 * TipoSearch represents the model behind the search form about `common\models\nodo\NodoTipo`.
 */
class TipoSearch extends NodoTipo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'borrado'], 'integer'],
            [['nombre', 'descripcion', 'folder'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NodoTipo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'borrado' => $this->borrado,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'folder', $this->folder]);

        return $dataProvider;
    }
}
