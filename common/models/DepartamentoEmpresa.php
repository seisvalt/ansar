<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "departamento_empresa".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Colaborador[] $colaboradors
 */
class DepartamentoEmpresa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'departamento_empresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColaboradors()
    {
        return $this->hasMany(Colaborador::className(), ['id_departamento_empresa' => 'id']);
    }
}
