<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "datos_generales".
 *
 * @property integer $id
 * @property integer $equvlencia_cargo
 */
class DatosGenerales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'datos_generales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equvlencia_cargo'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'equvlencia_cargo' => 'Equvlencia Cargo',
        ];
    }
}
