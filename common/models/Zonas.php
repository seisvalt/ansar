<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zonas".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property integer $padre
 * @property integer $izquierda
 * @property integer $derecha
 * @property integer $lvl
 * @property integer $borrado
 */
class Zonas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['padre', 'izquierda', 'derecha', 'lvl', 'borrado'], 'integer'],
            [['nombre', 'descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'padre' => 'Padre',
            'izquierda' => 'Izquierda',
            'derecha' => 'Derecha',
            'lvl' => 'Lvl',
            'borrado' => 'Borrado',
        ];
    }
}
