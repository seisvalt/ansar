<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\nodo\NodoTipo */

$this->title = 'Create Nodo Tipo';
$this->params['breadcrumbs'][] = ['label' => 'Nodo Tipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nodo-tipo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
