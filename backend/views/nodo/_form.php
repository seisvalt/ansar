<?php

use common\models\nodo\NodoTipo;
use common\models\Zonas;
use yii\helpers\ArrayHelper;
//use yii\helpers\Html;
use kartik\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\nodo\Nodo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nodo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'latitud')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'longitud')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => 255]) ?>

    <?=
        // Normal select with ActiveForm & model
        $form->field($model, 'id_zona')->widget(Select2::classname(), [
            'data' => array_merge(["" => ""], ArrayHelper::map(Zonas::find()->all(), 'id', 'nombre')),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione una zona ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>

    <?=
        // Normal select with ActiveForm & model
        $form->field($model, 'id_tipo')->widget(Select2::classname(), [
            'data' => array_merge(["" => ""], ArrayHelper::map(NodoTipo::find()->all(), 'id', 'nombre')),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione una zona ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>


<?php
    // Usage with ActiveForm and model
    echo $form->field($model, 'borrado')->widget(SwitchInput::classname(), [
    'type' => SwitchInput::CHECKBOX,'pluginOptions'=>[  'threeState'=>false,
            'labelText'=>'<i class="glyphicon glyphicon-trash"></i>',
            'onText'=>'<i class="glyphicon glyphicon-ok"></i>',
            'offText'=>'<i class="glyphicon glyphicon-remove"></i>'
        ]
    ]);


    ?>

    <?= $form->field($model, 'proyectos')->textInput(['maxlength' => 255]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
