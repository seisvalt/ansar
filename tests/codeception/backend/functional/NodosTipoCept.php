<?php

use tests\codeception\backend\FunctionalTester;
//use tests\codeception\common\_pages\LoginPage;

$I = new FunctionalTester($scenario);
$I->wantTo('ensure nodo tipo page works');
$I->amOnPage('/backend/web/index-test.php?r=nodo/tipo/index');
//$loginPage = LoginPage::openBy($I);

$I->see('Nodo Tipos','h1');
$I->click('Create Nodo Tipo');
$I->see('Create Nodo Tipo');
$I->dontSee('ID');