<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_142621_nodo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nodo}}', [
            'id' => Schema::TYPE_PK,
            'nombre' => Schema::TYPE_STRING . ' NOT NULL',
            'descripcion' => Schema::TYPE_STRING,
            'latitud' => Schema::TYPE_DECIMAL,
            'longitud' => Schema::TYPE_DECIMAL,
            'direccion' => Schema::TYPE_STRING,
            'id_zona' => Schema::TYPE_INTEGER,
            'proyectos' => Schema::TYPE_STRING,
            'estado' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',

        ], $tableOptions);

        // Add Foreign Keys Relations for RESERVED
        $this->addForeignKey("fk_nodo_tipo_idr", "nodo", "id_zona", "zonas", "id", "RESTRICT", "NO ACTION");

    }

    public function down()
    {
        $this->dropTable('{{%nodo}}');
    }
}
