<?php

use yii\db\Schema;
use yii\db\Migration;

class m150119_150840_tipo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nodo_tipo}}', [
            'id' => Schema::TYPE_PK,
            'nombre' => Schema::TYPE_STRING . ' NOT NULL',
            'descripcion' => Schema::TYPE_STRING,
            'folder' => Schema::TYPE_STRING,
            'borrado' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%nodo_tipo}}');
    }
}
