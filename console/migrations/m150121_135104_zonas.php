<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_135104_zonas extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%zonas}}', [
            'id' => Schema::TYPE_PK,
            'nombre' => Schema::TYPE_STRING . ' NOT NULL',
            'descripcion' => Schema::TYPE_STRING,
            'padre' => Schema::TYPE_INTEGER,
            'izquierda' => Schema::TYPE_INTEGER,
            'derecha' => Schema::TYPE_INTEGER,
            'lvl' => Schema::TYPE_SMALLINT,
            'borrado' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',

        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%zonas}}');
    }
}
