<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use miloschuman\highcharts\Highcharts;
use seisvalt\listas\Lista_simple;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Lista_simple::widget(["message"=>"joseeee"]) ?>
<?= @seisvalt\listas\Lista_simple::widget(["message"=>"meno"])?>
<?= Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart'=> [
            'zoomType'=> 'x',
            'height'=> 158,
            'backgroundColor'=> null,
            'spacingBottom'=> 15,
            'spacingTop'=> 5,
            'spacingLeft'=> 5,
            'spacingRight'=> 0
        ],
        'title' => [
            'text' => '',
        ],
        'credits'=>['enabled'=>false],
        'xAxis' => [
            'type'=>'datetime',

            'gridLineColor'=> "#ECECEC",
            'lineColor'=> "#999999",
            'gridLineWidth'=> 1,
        ],

        'yAxis'=>[
            'title'=> [
                'text'=> null
            ],
            'gridLineColor'=> '#ECECEC',
            'lineColor'=> '#999999',
            'lineWidth'=> 1,
        ],
        'legend'=>['enabled'=> false],
        'plotOptions'=> [
            'area'=> [
                'fillColor'=> [
                    'linearGradient'=> ['x1'=> 0, 'y1'=> 0, 'x2'=> 0, 'y2'=> 1],
                    'stops'=> [
                        [0, 'rgb(204, 204, 204)'],
                        [1, 'rgb(246, 246, 246)']
                    ]
                ],

                'marker'=> [
                    'radius'=> 2
                ],
                'lineWidth'=> 1,
                'states'=> [
                    'hover'=> [
                        'lineWidth'=> 1
                    ]
                ],
                'threshold'=> null
            ],
            'series'=> [
                'color'=> '#666',
            ]
        ],
        'series' => [
            [
                'type' => 'area',
                'name' => 'Horas',
                'pointInterval'=> 24 * 3600 * 1000,
                //	'pointStart'=> "Date.UTC(2014, 0, 1)",
                'data'=> [2, 7, 5, 10, 3, 9, 8, 4, 4, 4, 4, 5, 6, 7, 8, 9, 5, 6, 7, 8, 9]
            ],


        ],
    ]
]);
?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
