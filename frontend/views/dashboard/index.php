<?php
/* @var $this yii\web\View */
use seisvalt\listas\Lista_simple;
use seisvalt\map\Map;
$this->title = 'My Yii Application';
?>
<style>
 .container{
    padding: 0px;
 }
</style>
<div id="map"></div>
<div class="contenedor">
    <div class="sub-contenedor">
        <div class="menu-lateral" id="myMenu">
            <a href="#" id="menuBtn"><i class="fa fa-angle-double-right"></i></a>
            <?= Lista_simple::widget(["data"=>$nodos]) ?>

        </div>
        <div class="contenedor-mapa">
            <div id="map-canvas" style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;">
                <?= Map::widget(["data"=>$nodos]) ?>

            </div>
        </div>
    </div>
</div>
<?= $this->renderAjax('pie');?>
<!-- Inicio arreglo boton ocultar listado de sensores -->
<script>
    $(function() {
        var menuVisible = true;
        $('#menuBtn').click(function() {
            if (menuVisible) {
                $("#myMenu").animate({width:30},200);
                $(".sub-contenedor").animate({'padding-right':'30'},200);
                $(".cont-top-lateral").css({'display':'none'});
                $(".contenedor-scroll").css({'display':'none'});
                $("#menuBtn i").removeClass("fa fa-angle-double-right");
                $("#menuBtn i").addClass("fa fa-angle-double-left");
                $("#menuBtn").addClass('activo');
                menuVisible = false;
                return;
            }
            $("#myMenu").animate({width:320},200);
            $(".sub-contenedor").animate({'padding-right':'320'},200);
            $(".cont-top-lateral").css({'display':'block'});
            $(".contenedor-scroll").css({'display':'block'});
            $("#menuBtn i").removeClass("fa fa-angle-double-left");
            $("#menuBtn i").addClass("fa fa-angle-double-right");
            $("#menuBtn").removeClass('activo');
            menuVisible = true;
        });
    });
</script>
<!-- Fin arreglo boton ocultar listado de sensores -->