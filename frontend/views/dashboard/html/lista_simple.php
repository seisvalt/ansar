<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 16/03/15
 * Time: 09:09 AM
 */


?>

<div class="cont-top-lateral">
	<div class="titulo">
		<div class="titulo-sensores">sensores</div>
		<div class="filtros">
			<!-- <div>FILTROS:</div> -->
			<button type="button" class="btn btn-default">TODOS</button>
			<button type="button" class="btn btn-default">ONLINE</button>
			<button type="button" class="btn btn-default active">OFFLINE</button>
		</div>
	</div>
	<div class="cont-busqueda">
		<input class="input-buscar" type="text" name="" value="" >
		<i class="fa fa-search icono-buscar"></i>
	</div>
</div>
<div class="contenedor-scroll">
	<a class="item-sensor" href="#">
		<i class="fa fa-circle sensor-online estatus-item"></i>
		<div class="datos-item">
			<div class="nombre-sensor">Sensor Primario</div>
			<div class="ubicacion-sensor"><i class="fa fa-map-marker"></i><span>Las flores, Palmira, Valle del cauca, Colombia</span></div>
		</div>
	</a>
	<a class="item-sensor" href="#">
		<i class="fa fa-circle-o sensor-offline estatus-item"></i>
		<div class="datos-item">
			<div class="nombre-sensor">Sensor Las Americas</div>
			<div class="ubicacion-sensor"><i class="fa fa-map-marker"></i><span>Las flores, Palmira, Valle del cauca, Colombia</span></div>
		</div>
	</a>
	<a class="item-sensor" href="#">
		<i class="fa fa-circle sensor-online estatus-item"></i>
		<div class="datos-item">
			<div class="nombre-sensor">Palmira Norte</div>
			<div class="ubicacion-sensor"><i class="fa fa-map-marker"></i><span>Las flores, Palmira, Valle del cauca, Colombia</span></div>
		</div>
	</a>



</div>
