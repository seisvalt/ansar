<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 24/02/15
 * Time: 03:22 PM
 */
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\controls\ZoomControlStyle;
use dosamigos\google\maps\controls\ControlPosition;
use yii\web\JsExpression;



$coord = new LatLng(['lat' => 3.5200108, 'lng' => -76.278776]);
$zoomControl = array('style'=> ZoomControlStyle::SMALL, 'position'=> ControlPosition::RIGHT_BOTTOM);
$map = new Map([
	'center' => $coord,
	'zoom' => 16,
	'panControl'=>false,
	'zoomControl'=>true,
	'zoomControlOptions'=>new JsExpression('{style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.RIGHT_BOTTOM}'),
	'mapTypeControl'=>false,
	'scaleControl'=>false,
	'streetViewControl'=>false,
	'overviewMapControl'=>false,
	'rotateControl'=>true,
	'width'=>"100%",
	'height'=>"100%",
	'containerOptions'=>["class"=>"cositaseria", "style"=> "left: 0px; top: 0px; overflow: hidden;"],
//	'mapTypeId'=> MapTypeId::ROADMAP,
	'styles'=> json_decode(json_encode('[
				{
					"featureType": "water",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#b5cbe4"
						}
					]
				},
				{
					"featureType": "landscape",
					"stylers": [
						{
							"color": "#efefef"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#83a5b0"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#bdcdd3"
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e3eed3"
						}
					]
				},
				{
					"featureType": "administrative",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"lightness": 33
						}
					]
				},
				{
					"featureType": "road"
				},
				{
					"featureType": "poi.park",
					"elementType": "labels",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"lightness": 20
						}
					]
				},
				{},
				{
					"featureType": "road",
					"stylers": [
						{
							"lightness": 20
						}
					]
				}
			]'))
]);

// Lets add a marker now
$marker = new Marker([
	'position' => $coord,
	'title' => 'My Home Town',
]);

$contentString = '<div class="tooltip1">'.
	'<button type="button" class="btn btn-default pull-right">EXPLORAR</button>'.
	'<div class="nombre-sensor-tooltip">NOMBRE SENSOR</div>'.
	'<div><i class="fa fa-map-marker"></i> Las flores, Palmira, Valle del cauca, Colombia.</div>'.
	'<div><i class="fa fa-circle"></i> Online / Indoor</div>'.
	'<div>ULTIMA ACTUALIZACIÓN: DIC. 12 2014, 16:04:00 UTC</div>'.
	'<div class="tipos-sensores">'.
	'<div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'</div>'.
	'<div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'.
	'</div>'.
	'</div>'.
	'</div>';
// Provide a shared InfoWindow to the marker
$marker->attachInfoWindow(
	new InfoWindow([
		'content' => $contentString
	])
);
$map->addOverlay($marker);
echo $map->display();