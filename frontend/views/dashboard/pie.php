<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 23/02/15
 * Time: 03:03 PM
 */
use miloschuman\highcharts\Highcharts;
?>

<div class="grafica">
	<div class="datos-grafica" >
		<div class="actualizacion">
			<span>ULTIMA ACTUALIZACIÓN:</span>
			<span class="fecha">EN UN MES</span>
		</div>
		<div class="icono-grande">
			<i class="fa fa-bolt icono-grande-sensor"></i>
			<div class="valor-grafica">
				<div class="nombre-tipo">LUZ</div>
				<div class="numero">57.9<span class="tipo-medida">%</span></div>
			</div>
		</div>
		<div class="filtro">
			<div>
				ELEGIR SENSOR:
			</div>
			<div>
				<a ><i class="fa fa-sun-o"></i></a>
				<a class="active"><i class="fa fa-bolt"></i></a>
				<a ><i class="fa fa-fire"></i></a>
				<a ><i class="fa fa-cloud"></i></a>
				<a ><i class="fa fa-volume-up"></i></a>
			</div>
			<div>
				<a ><i class="fa fa-leaf"></i></a>
				<a ><i class="fa fa-wifi"></i></a>
				<a ><i class="fa fa-tint"></i></a>
				<a ><i class="fa fa-rocket"></i></a>
				<a ><i class="fa fa-tree"></i></a>
			</div>
		</div>
	</div>
	<div class="grafica-real" id="dgrap">
		<div class="ubicacion pull-left">
			<i class="fa fa-circle"></i>
			Palmira, Valle del cauca, Colombia
		</div>
		<div class="select pull-right">
			MOSTRAR ULTIMA:
			<select title="hola">
				<option value="">HORA</option>
				<option value="">DÍA</option>
				<option value="">MES</option>
				<option value="">AÑO</option>
			</select>
		</div>
		<div id="container1" >
			<?= Highcharts::widget([
				'scripts' => [
					'modules/exporting',
					'themes/grid-light',
				],
				'options' => [
					'chart'=> [
						'zoomType'=> 'x',
						'height'=> 158,
						'backgroundColor'=> null,
						'spacingBottom'=> 15,
						'spacingTop'=> 5,
						'spacingLeft'=> 5,
						'spacingRight'=> 0
					],
					'title' => [
						'text' => '',
					],
					'credits'=>['enabled'=>false],
					'xAxis' => [
						'type'=>'datetime',
						'gridLineColor'=> "#ffffff",
						'lineColor'=> "#ffffff",
						'gridLineWidth'=>0,
						'lineWidth'=> 0,
					],

					'yAxis'=>[
						'title'=> [
							'text'=> null
						],
						'gridLineColor'=> '#ffffff',
						'lineColor'=> '#ffffff',
						'gridLineWidth'=>0,
						'lineWidth'=> 0
					],
					'legend'=>['enabled'=> false],
					'plotOptions'=> [
						'area'=> [
							'fillColor'=> [
								'linearGradient'=> ['x1'=> 0, 'y1'=> 0, 'x2'=> 0, 'y2'=> 1],
								'stops'=> [
									[0, 'rgb(204, 204, 204)'],
									[1, 'rgb(246, 246, 246)']
								],
							],

							'marker'=> [
								'radius'=> 2
							],
							'lineWidth'=> 1,
							'states'=> [
								'hover'=> [
									'lineWidth'=> 1
								]
							],
							'threshold'=> null
						],
						'series'=> [
							'color'=> '#666',
						],
					],
					'series' => [
						[
							'type' => 'area',
							'name' => 'Horas',
							'pointInterval'=> 24 * 3600 * 1000,
						//	'pointStart'=> "Date.UTC(2014, 0, 1)",
							'data'=> [2, 7, 5, 10, 3, 9, 8, 4, 4, 4, 4, 5, 6, 7, 8, 9, 5, 6, 7, 8, 9]
						],


					],
				]
			]);
			?>
		</div>
	</div>

</div>