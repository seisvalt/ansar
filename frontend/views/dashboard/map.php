<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 5/02/15
 * Time: 09:23 AM
 */

?>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="https://google-maps-utility-library-v3.googlecode.com/svn-history/r391/trunk/markerwithlabel/src/markerwithlabel.js"></script>

<script>
	var myLatlng = new google.maps.LatLng(3.5200108,-76.278776);
	var myLatlng1 = new google.maps.LatLng(37.4338887,25.3449979);
	var myLatlng2 =	new google.maps.LatLng(-4.6237042,55.4538957);
	var sensoresOnline = [
		[3.5200108,-76.278776],
		[3.5200154,-76.2800352],
		[3.5196222,-76.2791514],
		[3.5198223,-76.2785787],
		[3.519978,-76.2795866]
	];
	var sensoresOffline = [
		[3.5219833,-76.2833771],
		[3.5212437,-76.2819568],
		[3.5209485,-76.281698],
		[3.5194129,-76.2802865],
		[3.5196438,-76.2795154]
	];
/* ----------------- INICIO Dibuja controles --------------------------------------------------------- */
	function CenterControl(controlDiv, map) {
		var controlUI = document.createElement('div');
		controlUI.style.backgroundColor = 'rgba(51,51,51,.7)';
		controlUI.style.marginBottom = '10px';
		controlUI.style.marginLeft = '-2px';
		controlDiv.appendChild(controlUI);

		var controlText = document.createElement('div');
		controlText.style.color = 'rgb(255,255,255)';
		controlText.style.fontFamily = 'light';
		controlText.style.fontSize = '13px';
		controlText.style.lineHeight = '20px';
		controlText.style.paddingLeft = '5px';
		controlText.style.paddingRight = '5px';
		controlText.style.display = 'inline-block';
		controlText.innerHTML = 'FILTROS';
		controlUI.appendChild(controlText);

		var circulo = document.createElement('div');
		circulo.style.backgroundColor = 'rgba(255,255,255,1)';
		circulo.style.width = '15px';
		circulo.style.height = '15px';
		circulo.style.display = 'inline-block';
		circulo.style.borderRadius = '10px';
		circulo.style.cursor = 'pointer';
		circulo.style.marginRight = '5px';
		circulo.style.marginBottom = '-2px';
		controlUI.appendChild(circulo);

		var circulo1 = document.createElement('div');
		circulo1.style.backgroundColor = '#00C266';
		circulo1.style.width = '15px';
		circulo1.style.height = '15px';
		circulo1.style.display = 'inline-block';
		circulo1.style.borderRadius = '10px';
		circulo1.style.cursor = 'pointer';
		circulo1.style.marginRight = '5px';
		circulo1.style.marginBottom = '-2px';
		controlUI.appendChild(circulo1);

		var circulo2 = document.createElement('div');
		circulo2.style.backgroundColor = '#ffffff';
		circulo2.style.width = '15px';
		circulo2.style.height = '15px';
		circulo2.style.display = 'inline-block';
		circulo2.style.borderRadius = '10px';
		circulo2.style.cursor = 'pointer';
		circulo2.style.marginRight = '20px';
		circulo2.style.marginBottom = '-2px';
		controlUI.appendChild(circulo2);

		var circulo3 = document.createElement('div');
		circulo3.style.backgroundColor = 'rgba(51,51,51,.7)';
		circulo3.style.width = '11px';
		circulo3.style.height = '11px';
		circulo3.style.marginTop = '2px';
		circulo3.style.marginLeft = '2px';
		circulo3.style.borderRadius = '10px';
		circulo3.style.cursor = 'pointer';
		circulo2.appendChild(circulo3);

		var controlText1 = document.createElement('div');
		controlText1.style.color = 'rgb(255,255,255)';
		controlText1.style.fontFamily = 'light';
		controlText1.style.fontSize = '13px';
		//controlText.style.float = 'left';
		//controlText1.style.lineHeight = '38px';
		controlText1.style.paddingTop = '5px';
		controlText1.style.paddingBottom = '5px';
		controlText1.style.paddingLeft = '5px';
		controlText1.style.paddingRight = '5px';
		controlText1.style.display = 'inline-block';
		controlText1.innerHTML = 'DISPOSITIVOS';
		controlUI.appendChild(controlText1);

		var circulo3 = document.createElement('div');
		circulo3.style.backgroundColor = 'rgba(255,255,255,1)';
		circulo3.style.color = 'rgb(51,51,51)';
		circulo3.style.padding = '0px 3px';
		circulo3.style.fontSize = '13px';
		circulo3.style.display = 'inline-block';
		circulo3.style.cursor = 'pointer';
		circulo3.style.marginRight = '10px';
		circulo3.innerHTML = '9999';
		controlUI.appendChild(circulo3);

		var controlText2 = document.createElement('div');
		controlText2.style.color = 'rgb(255,255,255)';
		controlText2.style.fontFamily = 'light';
		controlText2.style.fontSize = '13px';
		controlText2.style.paddingTop = '5px';
		controlText2.style.paddingBottom = '5px';
		controlText2.style.paddingLeft = '5px';
		controlText2.style.paddingRight = '5px';
		controlText2.style.display = 'inline-block';
		controlText2.innerHTML = 'SENSORES';
		controlUI.appendChild(controlText2);

		var circulo3 = document.createElement('div');
		circulo3.style.backgroundColor = 'rgba(255,255,255,1)';
		circulo3.style.color = 'rgb(51,51,51)';
		circulo3.style.padding = '0px 3px';
		circulo3.style.fontSize = '13px';
		circulo3.style.display = 'inline-block';
		circulo3.style.cursor = 'pointer';
		circulo3.style.marginRight = '10px';
		circulo3.innerHTML = '9999';
		controlUI.appendChild(circulo3);

		var controlText3 = document.createElement('div');
		controlText3.style.color = 'rgb(255,255,255)';
		controlText3.style.fontFamily = 'light';
		controlText3.style.fontSize = '13px';
		controlText3.style.paddingTop = '5px';
		controlText3.style.paddingBottom = '5px';
		controlText3.style.paddingLeft = '5px';
		controlText3.style.paddingRight = '5px';
		controlText3.style.display = 'inline-block';
		controlText3.innerHTML = 'PUNTOS';
		controlUI.appendChild(controlText3);

		var circulo3 = document.createElement('div');
		circulo3.style.backgroundColor = 'rgba(255,255,255,1)';
		circulo3.style.color = 'rgb(51,51,51)';
		circulo3.style.padding = '0px 3px';
		circulo3.style.fontSize = '13px';
		circulo3.style.display = 'inline-block';
		circulo3.style.cursor = 'pointer';
		circulo3.style.marginRight = '10px';
		circulo3.innerHTML = '9999';
		controlUI.appendChild(circulo3);

		google.maps.event.addDomListener(circulo, 'click', HideMarkersOnline);
		google.maps.event.addDomListener(circulo1, 'click', function() {
			map.setCenter(myLatlng1)
		});
		google.maps.event.addDomListener(circulo2, 'click', function() {
			map.setCenter(myLatlng2)
		});
	}
	function HideMarkersOnline() {
		map.setCenter(myLatlng);
  		// alert(sensoresOnline[0]);
  		// setAllMap(null);
  		// sensoresOnline[0].setMap(null);
  		// for (var i = 0; i < sensoresOnline.length; i++ ) {
  		// 	var p = "prueba"
  		// 	var num = i;
  		// 	var suma = p + num.toString();
  		// 	alert(suma);
  		// 	sensoresOnline[i].setMap(null);
  		// }
	}
/* ----------------- FIN Dibuja controles --------------------------------------------------------- */

/* ----------------- INICIO Configuracion mapa --------------------------------------------------------- */
	function initialize() {
		var mapOptions = {
			zoom: 16,
			center: new google.maps.LatLng(3.5199498,-76.2854921),
			panControl:false,
			zoomControl:true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			},
			mapTypeControl:false,
			scaleControl:false,
			streetViewControl:false,
			overviewMapControl:false,
			rotateControl:true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [
				{
					"featureType": "water",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#b5cbe4"
						}
					]
				},
				{
					"featureType": "landscape",
					"stylers": [
						{
							"color": "#efefef"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#83a5b0"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#bdcdd3"
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e3eed3"
						}
					]
				},
				{
					"featureType": "administrative",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"lightness": 33
						}
					]
				},
				{
					"featureType": "road"
				},
				{
					"featureType": "poi.park",
					"elementType": "labels",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"lightness": 20
						}
					]
				},
				{},
				{
					"featureType": "road",
					"stylers": [
						{
							"lightness": 20
						}
					]
				}
			]
		};
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
/* ----------------- INICIO Configuracion mapa --------------------------------------------------------- */

/* ----------------- INICIO Contenido del tooltip marker --------------------------------------------------------- */
		var contentString = '<div class="tooltip1">'+
			'<button type="button" class="btn btn-default pull-right">EXPLORAR</button>'+
			'<div class="nombre-sensor-tooltip">NOMBRE SENSOR</div>'+
			'<div><i class="fa fa-map-marker"></i> Las flores, Palmira, Valle del cauca, Colombia.</div>'+
			'<div><i class="fa fa-circle sensor-online"></i> Online / Indoor</div>'+
			'<div>ULTIMA ACTUALIZACIÓN: DIC. 12 2014, 16:04:00 UTC</div>'+
			'<div class="tipos-sensores">'+
			'<div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'</div>'+
			'<div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'<div><i class="fa fa-sun-o"></i><span class="nombres-tooltip">LUZ</span><span class="valor-tooltip">%</span></div>'+
			'</div>'+
			'</div>'+
			'</div>';
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
/* ----------------- FIN Contenido del tooltip marker --------------------------------------------------------- */

/* ----------------- INICIO Colocar puntos online --------------------------------------------------------- */
		for (var i = 0; i < sensoresOnline.length; i++) {
		    var myLatLng4 = new google.maps.LatLng(sensoresOnline[i][0], sensoresOnline[i][1]);
			// var marker = new google.maps.Marker({
			//     position: myLatLng4,
			//     map: map,
			//     title:"Hello World!"
			// });
			var marker = new MarkerWithLabel({
				position: myLatLng4,
			    icon: {
			      path: google.maps.SymbolPath.CIRCLE,
			      scale: 0,
			    },
			    map: map,
			    labelClass: "label",
			    labelAnchor: new google.maps.Point(10, 10)
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, this);
			});
		}
		for (var i = 0; i < sensoresOffline.length; i++) {
		    var myLatLng4 = new google.maps.LatLng(sensoresOffline[i][0], sensoresOffline[i][1]);
			// var marker = new google.maps.Marker({
			//     position: myLatLng4,
			//     map: map,
			//     title:"Hello World!"
			// });
			var marker = new MarkerWithLabel({
				position: myLatLng4,
			    icon: {
			      path: google.maps.SymbolPath.CIRCLE,
			      scale: 0,
			    },
			    map: map,
			    labelClass: "label1",
			    labelAnchor: new google.maps.Point(10, 10)
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, this);
			});
		}
/* ----------------- FIN Colocar puntos online --------------------------------------------------------- */

/* ----------------- INICIO Coloca los controles --------------------------------------------------------- */
		var centerControlDiv = document.createElement('div');
		var centerControl = new CenterControl(centerControlDiv, map);
		map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(centerControlDiv);
/* ----------------- FIN Coloca los controles --------------------------------------------------------- */

	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>